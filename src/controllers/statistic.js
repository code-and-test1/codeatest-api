const {response} = require('@codeatest/common')
const QuizStatistic = require('../actions/Statistic/QuizStatistic')

exports.countSubmissionsByQuiz = (req, res) => {
    const {quizId, ...query} = Object.assign({}, req.query)

    QuizStatistic.countSubmissionsByQuiz(quizId, query)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}


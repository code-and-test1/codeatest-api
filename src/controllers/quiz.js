const {response} = require('@codeatest/common')
const OAuth = require('../middlewares/OAuth')
const QuizAction = require('../actions/Quiz')
const TestCaseAction = require('../actions/TestCase')

exports.getList = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    QuizAction.getList(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getById = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    QuizAction.getById(quizId, field)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getRanking = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const {sort} = Object.assign({}, req.query)
    const authToken = OAuth.getBearerToken(req)

    QuizAction.getRanking(quizId, {token: authToken}, {sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.joinById = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const authPayload = OAuth.getPayloadToken(req)

    QuizAction.joinById(quizId, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.submission = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)
    const file = req.file
    const authPayload = OAuth.getPayloadToken(req)

    QuizAction.submission(quizId, _body, file, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.create = (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    QuizAction.create(_body, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createTestCase = (req, res) => {
    const _body = Object.assign({}, req.body)
    const {quiz_id: quizId} = Object.assign({}, req.params)

    QuizAction.createTestCase(quizId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getTestCases = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {quiz_id: quizId} = Object.assign({}, req.params)

    TestCaseAction.getList(Object.assign(query, {quiz: quizId}), {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateById = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    QuizAction.updateById(quizId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.deleteById = (req, res) => {
    const {quiz_id: quizId} = Object.assign({}, req.params)

    QuizAction.deleteById(quizId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

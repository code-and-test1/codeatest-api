const {response} = require('@codeatest/common')
const TopicAction = require('../actions/Topic')
const QuizAction = require('../actions/Quiz')
const OAuth = require('../middlewares/OAuth')

exports.getList = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    TopicAction.getList(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getById = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    TopicAction.getById(topicId, field)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.joinById = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)
    const authPayload = OAuth.getPayloadToken(req)

    TopicAction.joinById(topicId, field, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getQuizById = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    // const {field} = Object.assign({}, req.query)

    TopicAction.getQuizById(topicId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createQuiz = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    const _body = Object.assign({topic: topicId}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    QuizAction.create(_body, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}
exports.getRanking = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    const {sort} = Object.assign({}, req.query)
    const authToken = OAuth.getBearerToken(req)

    TopicAction.getRanking(topicId, {token: authToken}, {sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.create = (req, res) => {
    const _body = Object.assign({}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    TopicAction.create(_body, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateById = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    TopicAction.updateById(topicId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.deleteById = (req, res) => {
    const {topic_id: topicId} = Object.assign({}, req.params)


    TopicAction.deleteById(topicId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

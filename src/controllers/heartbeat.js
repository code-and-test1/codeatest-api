const {getModel} = require('../connections/database')
const {response} = require('@codeatest/common')

const Heartbeat = getModel('Heartbeat')

exports.ping = (req, res) => {
    res.status(200).send(process.env.PROJECT_NAME || 'API')
}

exports.ready = (req, res) => {
    Heartbeat.findOne().lean()
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}


const {response} = require('@codeatest/common')
const WorkerAction = require('../actions/Worker')

exports.getListWorker = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    WorkerAction.getListWorker(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getSupportedLanguages = (req, res) => {
    Promise.resolve({data: ['js', 'py', 'go', 'c', 'cpp']})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getWorkerById = (req, res) => {
    const {worker_id: WorkerId} = Object.assign({}, req.params)

    WorkerAction.getWorkerById(WorkerId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createWorker = (req, res) => {
    const _body = Object.assign({}, req.body)

    WorkerAction.createWorker(_body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateWorkerById = (req, res) => {
    const {worker_id: workerId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    WorkerAction.updateWorkerById(workerId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

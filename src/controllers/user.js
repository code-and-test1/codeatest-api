const {response} = require('@codeatest/common')
const UserAction = require('../actions/User')

exports.getList = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    UserAction.getList(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getTopic = (req, res) => {
    const {page, limit, field, sort} = Object.assign({}, req.query)
    const {user_id: userId} = Object.assign({}, req.params)

    UserAction.getTopic(userId, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getQuizByTopic = (req, res) => {
    const {field, sort} = Object.assign({}, req.query)
    const {user_id: userId, topic_id: topicId} = Object.assign({}, req.params)

    UserAction.getQuizByTopic(userId, topicId, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getAllSubmission = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {user_id: userId} = Object.assign({}, req.params)

    UserAction.getAllSubmission({query, userId}, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getSubmissionOfQuiz = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {user_id: userId, quiz_id: quizId} = Object.assign({}, req.params)

    UserAction.getSubmissionOfQuiz({query, userId, quizId}, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

// exports.getById = (req, res) => {
//     const {user_id: userId} = Object.assign({}, req.params)

//     UserAction.getById(userId)
//         .then(response.sendSuccess(req, res))
//         .catch(response.sendError(req, res))
// }

exports.create = (req, res) => {
    const _body = Object.assign({}, req.body)

    UserAction.create(_body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

// exports.updateById = (req, res) => {
//     const {user_id: userId} = Object.assign({}, req.params)
//     const _body = Object.assign({}, req.body)

//     UserAction.updateById(userId, _body)
//         .then(response.sendSuccess(req, res))
//         .catch(response.sendError(req, res))
// }

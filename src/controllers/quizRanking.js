const {response} = require('@codeatest/common')
const RankingQuiz = require('../actions/Statistic/RankingQuiz')

exports.getTopScoreByQuiz = (req, res) => {
    const {quizId, ...query} = Object.assign({}, req.query)

    RankingQuiz.getTopScoreByQuiz(quizId, query)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}
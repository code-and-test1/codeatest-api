const {response} = require('@codeatest/common')
const CategoryAction = require('../actions/Category')
const TopicAction = require('../actions/Topic')
const OAuth = require('../middlewares/OAuth')

exports.getListCategory = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    CategoryAction.getListCategory(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getTopics = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {category_id: categoryId} = Object.assign({}, req.params)

    CategoryAction.getTopics(categoryId, query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createTopic = (req, res) => {
    const {category_id: categoryId} = Object.assign({}, req.params)
    const _body = Object.assign({category: categoryId}, req.body)
    const authPayload = OAuth.getPayloadToken(req)

    TopicAction.create(_body, authPayload)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getCategoryById = (req, res) => {
    const {category_id: categoryId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    CategoryAction.getCategoryById(categoryId, field)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getCategoryGeneral = (req, res) => {
    const {category_id: categoryId} = Object.assign({}, req.params)

    CategoryAction.getCategoryGeneral(categoryId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createCategory = (req, res) => {
    const _body = Object.assign({}, req.body)

    CategoryAction.createCategory(_body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateCategoryById = (req, res) => {
    const {category_id: categoryId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    CategoryAction.updateCategoryById(categoryId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.deleteById = (req, res) => {
    const {category_id: categoryId} = Object.assign({}, req.params)

    CategoryAction.deleteById(categoryId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

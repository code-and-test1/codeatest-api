const {response} = require('@codeatest/common')
const JobAction = require('../actions/Job')

exports.getListJob = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    JobAction.getListJob(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getJobById = (req, res) => {
    const {job_id: jobId} = Object.assign({}, req.params)

    JobAction.getJobById(jobId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createJob = (req, res) => {
    const _body = Object.assign({}, req.body)

    JobAction.createJob(_body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateJobById = (req, res) => {
    const {job_id: jobId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    JobAction.updateJobById(jobId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

const {response} = require('@codeatest/common')
const RankingTopic = require('../actions/Statistic/RankingTopic')

exports.getTopScoreByTopic = (req, res) => {
    const {topicId, ...query} = Object.assign({}, req.query)

    RankingTopic.getTopScoreByTopic(topicId, query)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

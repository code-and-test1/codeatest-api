const {response} = require('@codeatest/common')
const TestCaseAction = require('../actions/TestCase')

exports.getList = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    TestCaseAction.getList(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getById = (req, res) => {
    const {test_case_id: testCaseId} = Object.assign({}, req.params)

    TestCaseAction.getById(testCaseId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.create = (req, res) => {
    const _body = Object.assign({}, req.body)

    TestCaseAction.create(_body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateById = (req, res) => {
    const {test_case_id: testCaseId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    TestCaseAction.updateById(testCaseId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.deleteById = (req, res) => {
    const {test_case_id: testCaseId} = Object.assign({}, req.params)

    TestCaseAction.deleteById(testCaseId)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

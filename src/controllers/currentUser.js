const {response} = require('@codeatest/common')
const UserAction = require('../actions/User')
const OAuth = require('../middlewares/OAuth')

exports.getTopic = (req, res) => {
    const {page, limit, field, sort} = Object.assign({}, req.query)
    const {user_id: userId} = OAuth.getPayloadToken(req)

    UserAction.getTopic(userId, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getQuizByTopic = (req, res) => {
    const {field, sort} = Object.assign({}, req.query)
    const {topic_id: topicId} = Object.assign({}, req.params)
    const {user_id: userId} = OAuth.getPayloadToken(req)

    UserAction.getQuizByTopic(userId, topicId, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getAllSubmission = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {user_id: userId} = OAuth.getPayloadToken(req)

    UserAction.getAllSubmission({query, userId}, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getSubmissionOfQuiz = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {quiz_id: quizId} = Object.assign({}, req.params)
    const {user_id: userId} = OAuth.getPayloadToken(req)

    UserAction.getSubmissionOfQuiz({query, userId, quizId}, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

const {response} = require('@codeatest/common')
const SubmitAction = require('../actions/Submit')
const JobAction = require('../actions/Job')

exports.getListSubmit = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)

    SubmitAction.getList(query, {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getSubmitById = (req, res) => {
    const {submit_id: SubmitId} = Object.assign({}, req.params)
    const {field} = Object.assign({}, req.query)

    SubmitAction.getById(SubmitId, field)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.getListJob = (req, res) => {
    const {page, limit, field, sort, ...query} = Object.assign({}, req.query)
    const {submit_id: submitId} = Object.assign({}, req.params)

    JobAction.getListJob(Object.assign(query, {submit: submitId}), {page, limit}, {field, sort})
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.createSubmit = (req, res) => {
    const _body = Object.assign({}, req.body)
    const file = req.file

    SubmitAction.create(_body, file)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

exports.updateSubmitById = (req, res) => {
    const {submit_id: submitId} = Object.assign({}, req.params)
    const _body = Object.assign({}, req.body)

    SubmitAction.updateById(submitId, _body)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}

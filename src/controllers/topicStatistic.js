const {response} = require('@codeatest/common')
const TopicStatistic = require('../actions/Statistic/TopicStatistic')

exports.countSubmissionsByTopic = (req, res) => {
    const {topicId, ...query} = Object.assign({}, req.query)

    TopicStatistic.countSubmissionsByTopic(topicId, query)
        .then(response.sendSuccess(req, res))
        .catch(response.sendError(req, res))
}


const Uploader = require('@codeatest/uploader')
const path = require('path')

const DEFAULT_CREDENTIALS = path.join(__dirname, './code-and-test-1a7e7a4f4c05.json')
// const DEFAULT_PROJECT_ID = 'code-and-t'
// const DEFAULT_BUCKET_NAME = 'codeatest'
const DEFAULT_PROJECT_ID = 'blissful-star-290200'
const DEFAULT_BUCKET_NAME = 'codeatest-2'

const uploader = new Uploader({
    credentials: DEFAULT_CREDENTIALS,
    projectId: DEFAULT_PROJECT_ID,
    bucketName: DEFAULT_BUCKET_NAME,
})

module.exports = uploader

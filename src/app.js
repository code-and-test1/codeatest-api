const express = require('express')
const app = express()
const errorHandler = require('errorhandler')
const bodyParser = require('body-parser')
const compression = require('compression')
const cors = require('cors')
const logger = require('morgan')
const swaggerUI = require('swagger-ui-express')
const {response} = require('@codeatest/common')

/**
 * Express configuration.
 */
app.set('trust proxy', 1)
app.disable('x-powered-by')
app.use(compression())
app.use(bodyParser.json({limit: '5mb'}))
app.use(bodyParser.urlencoded({extended: true, limit: '5mb'}))
app.use(errorHandler())
app.use(cors())
app.use(logger('dev'))
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(require('../swagger.json'), {explorer: true}))
app.use((error, req, res, _) => {
    return response.sendError(req, res)(error)
})


setTimeout(async () => {
    /**
     * Config routes.
     */
    app.use(require('./app.routes'))

    const PORT = process.env.PORT || 1900
    console.log(PORT)

    const server = require('http').createServer(app)
    server.listen(PORT, () => {
        console.info(`Listening on port ${PORT}...`)
    })

}, 0)

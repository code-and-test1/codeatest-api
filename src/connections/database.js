const {connector} = require('@codeatest/common')
const database = require('@codeatest/code-and-test-schemas')

const MONGODB_URI = process.env.MONGODB_URI
console.log('MONGODB_URI:', MONGODB_URI)

const originConnection = connector.MongoDB(MONGODB_URI, {
    poolSize: 5,
    debug: false
})

module.exports = database(originConnection)

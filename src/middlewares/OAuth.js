const jwt = require('jsonwebtoken')

const decodeToken = (token) => {

    return jwt.decode(token, {complete: true})
}

const getBearerToken = (req) => {
    const fromAuthorizationHeader = req.get('Authorization')
    const fromApiTokenHeader = req.get('api-token')

    const rawToken = (fromAuthorizationHeader || fromApiTokenHeader) || ''

    if (rawToken) {
        return rawToken.replace('Bearer ', '').trim()
    }

    return ''
}

const getPayloadToken = (req) => {
    return req['_payload_token']
}

const getHeaderToken = (req) => {
    return req['_header_token']
}

const injectPayloadToken = (req, res, next) => {
    const token = getBearerToken(req)

    if (!token) {
        res.status(403).send('No token provided.')

        return
    }

    const decodedToken = decodeToken(token)

    req['_payload_token'] = decodedToken.payload
    req['_header_token'] = decodedToken.header

    next()
}

const send403Forbidden = (res) => {
    res.status(403).send('403 Forbidden. codeatest@2020')
}

const parseRole = (listRoles) => {
    if (!listRoles) return []

    const roles = listRoles.split('|').filter(Boolean)

    return roles
}

const hasRole = (role) => (req, res, next) => {
    const payloadToken = Object.assign({}, getPayloadToken(req))

    const listRoles = payloadToken.list_roles

    if (!listRoles) {
        send403Forbidden(res)

        return
    }

    const roles = parseRole(listRoles)

    if (!roles.includes(role)) {
        send403Forbidden(res)

        return
    }

    next()
}

exports.getPayloadToken = getPayloadToken

exports.getHeaderToken = getHeaderToken

exports.injectPayloadToken = injectPayloadToken

exports.hasRole = hasRole

exports.getBearerToken = getBearerToken

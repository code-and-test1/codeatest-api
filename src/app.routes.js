const express = require('express')
const BasicAuth = require('express-basic-auth')
const router = express.Router()
const {response} = require('@codeatest/common')
const Multer = require('./middlewares/Multer')
const OAuth = require('./middlewares/OAuth')

router.get('/', (req, res) => res.send(`Hi! I'm ${process.env.PROJECT_NAME}`))
router.get('/ping', (req, res) => res.send(`${process.env.PROJECT_NAME}:pong`))

const heartbeatCtl = require('./controllers/heartbeat')
router.get('/heartbeat/ping', heartbeatCtl.ping)
router.get('/heartbeat/ready', heartbeatCtl.ready)

/**
 * OAuth Middleware
 */
router.use(OAuth.injectPayloadToken)

/**
 * Private Using Basic Auth
 */
const userCtrl = require('./controllers/user')
router.post('/users', BasicAuth({users: {'admin': 'codeatest_2020'}}), userCtrl.create)
router.get('/users/:user_id/topics', userCtrl.getTopic)
router.get('/users/:user_id/topic/:topic_id/quizzes', userCtrl.getQuizByTopic)
router.get('/users/:user_id/submissions', userCtrl.getAllSubmission)
router.get('/users/:user_id/quizzes/:quiz_id/submissions', userCtrl.getSubmissionOfQuiz)

router.get('/token', (req, res) => {

    res.status(200).json({payload: OAuth.getPayloadToken(req), header: OAuth.getHeaderToken(req)})
})

router.get('/checkadmin', OAuth.hasRole('GROUP_ADMIN/G4'), (req, res) => {

    res.status(200).json({payload: OAuth.getPayloadToken(req), header: OAuth.getHeaderToken(req)})
})

/**
 * Current user api
 */
const currentUserCtrl = require('./controllers/currentUser')
router.get('/me/topics', currentUserCtrl.getTopic)
router.get('/me/topic/:topic_id/quizzes', currentUserCtrl.getQuizByTopic)
router.get('/me/submissions', currentUserCtrl.getAllSubmission)
router.get('/me/quizzes/:quiz_id/submissions', currentUserCtrl.getSubmissionOfQuiz)

/**
 * Category.
 */
const categoryCtrl = require('./controllers/category')
router.get('/categories', categoryCtrl.getListCategory)
router.get('/categories/:category_id', categoryCtrl.getCategoryById)
router.get('/categories/:category_id/general', categoryCtrl.getCategoryGeneral)
router.get('/categories/:category_id/topics', categoryCtrl.getTopics)
router.post('/categories/:category_id/topics', OAuth.hasRole('GROUP_ADMIN/G4'), categoryCtrl.createTopic)
router.post('/categories', OAuth.hasRole('GROUP_ADMIN/G4'), categoryCtrl.createCategory)
router.put('/categories/:category_id', OAuth.hasRole('GROUP_ADMIN/G4'), categoryCtrl.updateCategoryById)
router.delete('/categories/:category_id', categoryCtrl.deleteById)

/**
 * Topic.
 */
const topicCtrl = require('./controllers/topic')
router.get('/topics', topicCtrl.getList)
router.get('/topics/:topic_id', topicCtrl.getById)
router.get('/topics/:topic_id/ranking', topicCtrl.getRanking)
router.post('/topics/:topic_id/join', topicCtrl.joinById)
router.get('/topics/:topic_id/quizzes', topicCtrl.getQuizById)
router.post('/topics/:topic_id/quizzes', OAuth.hasRole('GROUP_ADMIN/G4'), topicCtrl.createQuiz)
router.post('/topics', OAuth.hasRole('GROUP_ADMIN/G4'), topicCtrl.create)
router.put('/topics/:topic_id', OAuth.hasRole('GROUP_ADMIN/G4'), topicCtrl.updateById)
router.delete('/topics/:topic_id', topicCtrl.deleteById)

/**
 * Quiz.
 */
const quizCtrl = require('./controllers/quiz')
router.get('/quizzes', quizCtrl.getList)
router.get('/quizzes/:quiz_id', quizCtrl.getById)
router.get('/quizzes/:quiz_id/ranking', quizCtrl.getRanking)
router.post('/quizzes/:quiz_id/join', quizCtrl.joinById)
router.post('/quizzes/:quiz_id/submission', Multer.single('file'), quizCtrl.submission)
router.post('/quizzes', OAuth.hasRole('GROUP_ADMIN/G4'), quizCtrl.create)
router.post('/quizzes/:quiz_id/test-cases', OAuth.hasRole('GROUP_ADMIN/G4'), quizCtrl.createTestCase)
router.get('/quizzes/:quiz_id/test-cases', OAuth.hasRole('GROUP_ADMIN/G4'), quizCtrl.getTestCases)
router.put('/quizzes/:quiz_id', quizCtrl.updateById)
router.delete('/quizzes/:quiz_id', quizCtrl.deleteById)

/**
 * TestCase.
 */
const testCaseCtrl = require('./controllers/testCase')
router.get('/test-cases', testCaseCtrl.getList)
router.get('/test-cases/:test_case_id', testCaseCtrl.getById)
// router.post('/test-cases', testCaseCtrl.create)
router.put('/test-cases/:test_case_id', OAuth.hasRole('GROUP_ADMIN/G4'), testCaseCtrl.updateById)
router.delete('/test-cases/:test_case_id', testCaseCtrl.deleteById)

/**
 * Worker.
 */
const WorkerCtrl = require('./controllers/worker')
router.get('/workers', WorkerCtrl.getListWorker)
router.get('/workers/supported-langs', WorkerCtrl.getSupportedLanguages)
router.get('/workers/:worker_id', WorkerCtrl.getWorkerById)
router.post('/workers', OAuth.hasRole('GROUP_ADMIN/G4'), WorkerCtrl.createWorker)
router.put('/workers/:worker_id', OAuth.hasRole('GROUP_ADMIN/G4'), WorkerCtrl.updateWorkerById)

/**
 * Submit.
 */
const submitCtrl = require('./controllers/submit')
router.get('/submits', submitCtrl.getListSubmit)
router.get('/submits/:submit_id', submitCtrl.getSubmitById)
router.get('/submits/:submit_id/jobs', submitCtrl.getListJob)
// router.post('/submits', Multer.single('file'), submitCtrl.createSubmit)
router.put('/submits/:submit_id', submitCtrl.updateSubmitById)

/**
 * Job.
 */
const jobCtrl = require('./controllers/job')
router.get('/jobs', jobCtrl.getListJob)
router.get('/jobs/:job_id', jobCtrl.getJobById)
// router.post('/jobs', jobCtrl.createJob)
// router.put('/jobs/:job_id', jobCtrl.updateJobById)

/**
 * Statistic.
 */
const statisticCtrl = require('./controllers/statistic')
router.get('/statistic/quiz/count-submissions', statisticCtrl.countSubmissionsByQuiz)

const topicStatisticCtrl = require('./controllers/topicStatistic')
router.get('/statistic/topic/count-submissions', topicStatisticCtrl.countSubmissionsByTopic)

const topicRankingCtrl = require('./controllers/topicRanking')
router.get('/ranking/topic/top-topic', topicRankingCtrl.getTopScoreByTopic)

const quizRankingCtrl = require('./controllers/quizRanking')
router.get('/ranking/quiz/top-quiz', quizRankingCtrl.getTopScoreByQuiz)
/**
 * 404 page.
 */
router.get('*', response.send404)

/**
 * Exports.
 */
module.exports = router

const {validateString, validateBoolean} = require('../../helpers/parser')
const parseDateQuery = require('../../helpers/parseDateQuery')
// const {escapeStringRegex} = require('../../helpers/parseRegex')

module.exports = (query) => {
    const builtQuery = {}

    if (query.status) builtQuery.status = validateString(query.status)

    if (query.updated_at) builtQuery.updated_at = parseDateQuery(query.updated_at)

    if (query.created_at) builtQuery.created_at = parseDateQuery(query.created_at)

    return Object.assign(query, builtQuery)
}

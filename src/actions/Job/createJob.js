const {getModel} = require('../../connections/database')

const Job = getModel('Job')

const createOneJob = async job => {
    const newJob = new Job(job)

    const doc = await newJob.save()

    return doc.toJSON()
}


module.exports = async (job) => {

    const newJob = await createOneJob(job)

    return {
        data: newJob,
    }
}

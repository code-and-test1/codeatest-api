const {getModel} = require('../../connections/database')

const Job = getModel('Job')

const updateJobById = (_id, updateJob) => {
    return Job
        .findByIdAndUpdate(
            _id,
            Object.assign(updateJob, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, job) => {

    const newJob = await updateJobById(_id, job)

    if (!newJob) throw new Error(`Job ${_id} not exists`)

    return {
        data: newJob,
    }
}

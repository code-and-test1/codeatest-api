const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')

const Category = getModel('Category')

const _findCategoryById = (_id, select) => {
    const find = Category.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const foundCategory = await _findCategoryById(_id, parsedField)

    if (!foundCategory) throw new Error(`category ${_id} not found`)

    return {
        data: foundCategory,
        meta: {
            field: parsedField,
        }
    }
}

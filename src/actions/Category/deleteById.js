const {getModel} = require('../../connections/database')

const Category = getModel('Category')

const updateCategoryById = (_id, updateCategory) => {
    return Category
        .findByIdAndUpdate(
            _id,
            Object.assign(updateCategory, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id) => {
    const oldCategory = await Category.findById(_id).lean()

    if (!oldCategory) throw new Error(`Category:${_id} does not exists`)

    await updateCategoryById(_id, {
        is_deleted: true,
    })

    return {
        data: true,
    }
}

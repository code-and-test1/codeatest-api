const Checker = require('../../helpers/Checker')
const allowStatuses = ['active', 'in-active']

module.exports = (category) => {
    if (!category) return null

    const cat = Object.assign({}, category)

    if (category.name) {
        cat.name = Checker.isString(category.name) ? category.name.trim() : null
    }

    if (category.description) {

        cat.description = Checker.isString(category.description) ? category.description.trim() : null
    }

    if (category.status) {

        cat.status = allowStatuses.includes(category.status) ? category.status : null
    }

    if (category.slug) {

        cat.slug = Checker.isString(category.slug) ? category.slug.trim() : null
    }

    return cat
}

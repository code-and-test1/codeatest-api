const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildCategoryQuery = require('./buildCategoryQuery')

const Category = getModel('Category')

const findCategories = (query, {skip, limit}, {select, sort}) => {

    query.is_deleted = {$ne: true}

    const find = Category.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const countCategories = query => {
    return Category.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildCategoryQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [categories, total] = await Promise.all([
        findCategories(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countCategories(builtQuery),
    ])

    return {
        data: {
            categories: categories,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

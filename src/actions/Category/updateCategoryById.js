const parseCategory = require('./parseCategory')
const {getModel} = require('../../connections/database')

const Category = getModel('Category')

const updateCategoryById = (_id, updateCategory) => {
    return Category
        .findByIdAndUpdate(
            _id,
            Object.assign(updateCategory, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

const findOneCategory = (query) => {

    return Category.findOne(query).lean()
}

module.exports = async (_id, category) => {
    const parsedCategory = parseCategory(category)

    if (!parsedCategory) throw new Error(`empty category update`)

    console.log(parsedCategory)

    const query = []


    // const foundCategory = await findOneCategory({$or: [{name: parsedCategory.name}, {slug: parsedCategory.slug}]})
    // console.log(foundCategory)

    // if (foundCategory) throw new Error(`category has slug ${category.slug} or name ${category.name} exists`)

    const newCategory = await updateCategoryById(_id, parsedCategory)

    if (!newCategory) throw new Error(`category ${_id} not exists`)

    return {
        data: {
            category: newCategory,
        },
    }
}

const Checker = require('../../helpers/Checker')
const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')

const Category = getModel('Category')

const _findCategoryById = (_id, select) => {
    let find = Category.findById(_id)

    if (!Checker.isObjectId(_id)) {
        find = Category.findOne({
            slug: _id,
        })
    }

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const foundCategory = await _findCategoryById(_id, parsedField)

    if (!foundCategory) throw new Error(`category ${_id} not found`)

    return {
        data: {
            category: foundCategory
        },
        meta: {
            field: parsedField || 'all',
        }
    }
}

const Checker = require('../../helpers/Checker')
const mongoose = require('@codeatest/common').mongoose
const {getModel} = require('../../connections/database')

const Category = getModel('Category')
const Topic = getModel('Topic')
const UserTopic = getModel('UserTopic')

const _findCategoryById = (_id, select) => {
    let find = Category.findById(_id)

    if (!Checker.isObjectId(_id)) {
        find = Category.findOne({
            slug: _id,
        })
    }

    if (select) find.select(select)

    return find.lean()
}

const _getGeneral = async (categoryId) => {

    const topics = await Topic.find({
        category: categoryId
    }).select('_id name').lean()

    const data = await UserTopic.aggregate([
        {$match: {topic: {$in: topics.map(topic => mongoose.Types.ObjectId(topic._id))}}},
        {$group: {_id: {topic: "$topic"}, total_member: {$sum: 1}}},
    ])

    return topics.map(topic => {
        const {_id} = topic

        const sId = _id.toString()

        const generalOfTopic = data.find((agg) => {
            const {_id: {topic: topicId}} = agg

            const sTopicId = topicId.toString()

            return sTopicId === sId
        }) || {
            total_member: 0,
        }

        delete generalOfTopic._id

        return Object.assign(topic, generalOfTopic)
    })
}

module.exports = async (_id) => {
    const foundCategory = await _findCategoryById(_id)

    if (!foundCategory) throw new Error(`category ${_id} not found`)

    const general = await _getGeneral(foundCategory._id)

    return {
        data: {
            category: foundCategory,
            general: general,
        },
        meta: {
        }
    }
}

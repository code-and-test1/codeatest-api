const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildTopicQuery = require('../Topic/buildQuery')

const Category = getModel('Category')
const Topic = getModel('Topic')

const _findTopics = (query, {skip, limit}, {select, sort}) => {

    const find = Topic.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const _countTopics = query => {
    return Category.countDocuments(query)
}

const _findCategory = (categoryId) => {
    return Category.findById(categoryId).select('_id').lean()
}

module.exports = async (categoryId, query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildTopicQuery(query)

    builtQuery.category = categoryId
    builtQuery.is_deleted = {
        $ne: true,
    }

    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [category, topics, total] = await Promise.all([
        _findCategory(categoryId),
        _findTopics(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        _countTopics(builtQuery),
    ])

    if (!category) throw new Error(`Category ${categoryId} not found`)

    return {
        data: {
            topics: topics,
            category: category,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

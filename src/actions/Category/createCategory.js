const {getModel} = require('../../connections/database')
const parseCategory = require('./parseCategory')

const Category = getModel('Category')

const createOneCategory = async category => {
    const newCategory = new Category(category)

    const doc = await newCategory.save()

    return doc.toJSON()
}

const findOneCategory = (query) => {

    return Category.findOne(query).lean()
}

const prepareNewCategory = (category) => {
    const parsedCategory = parseCategory(category)

    if (!parsedCategory) throw new Error(`empty category to create`)

    if (!parsedCategory.name) throw new Error(`category.name is require`)

    if (!parsedCategory.slug) throw new Error(`category.slug is require`)

    parsedCategory.status = 'active'

    return parsedCategory
}


module.exports = async (category) => {
    const parsedCategory = prepareNewCategory(category)

    const foundCategory = await findOneCategory({$or: [{name: parsedCategory.name}, {slug: parsedCategory.slug}]})

    if (foundCategory) throw new Error(`category has slug ${category.slug} or name ${category.name} exists`)

    const newCategory = await createOneCategory(category)

    return {
        data: {
            category: newCategory
        },
        meta: {
            category
        },
    }
}

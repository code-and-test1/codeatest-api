const parseTopic = require('./parseData')
const {getModel} = require('../../connections/database')

const Topic = getModel('Topic')

const updateTopicById = (_id, updateTopic) => {
    return Topic
        .findByIdAndUpdate(
            _id,
            Object.assign(updateTopic, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, topic) => {
    const parsedTopic = parseTopic(topic)

    if (!parsedTopic) throw new Error(`empty Topic update`)

    const oldTopic = await Topic.findById(_id).lean()

    if (!oldTopic) throw new Error(`topic:${_id} does not exists`)

    const mergedTopic = Object.assign(oldTopic, parsedTopic)


    // const foundTopic = await Topic.findOne({name: mergedTopic.name, category: mergedTopic.category})
    
    // if (foundTopic) throw new Error('Topic exists')


    // if (foundTopic) throw new Error(`Topic ${name} in category:${category} exists`)

    const newTopic = await updateTopicById(_id, parsedTopic)

    if (!newTopic) throw new Error(`Topic ${_id} not exists`)

    return {
        data: {
            topic: newTopic,
        },
    }
}

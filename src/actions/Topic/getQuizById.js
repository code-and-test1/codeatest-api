const Promise = require('bluebird')
const {getModel} = require('../../connections/database')
const Topic = getModel('Topic')
const Quiz = getModel('Quiz')

const _findById = (_id) => {
    const find = Topic.findById(_id).select('_id')

    return find.lean()
}

const _getAllQuiz = (topic_id) => {

    return Quiz.find({
        topic: topic_id,
        is_deleted: {$ne: true},
    }).lean()
}

module.exports = async (_id) => {
    // const parsedField = parseField(field)

    const findTopic = _findById(_id)
    const findAllQuiz = _getAllQuiz(_id)

    const [topicFound, quizzes] = await Promise.all([findTopic, findAllQuiz])

    if (!topicFound) throw new Error(`Topic ${_id} not found`)

    return {
        data: {
            quizzes: quizzes,
        },
        // meta: {
        //     field: parsedField,
        // }
    }
}

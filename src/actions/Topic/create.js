const {getModel} = require('../../connections/database')
const parseTopic = require('./parseData')

const Topic = getModel('Topic')

const createOneTopic = async (topic, userId) => {
    topic.author = userId

    const newTopic = new Topic(topic)

    const doc = await newTopic.save()

    return doc.toJSON()
}

const findOneTopic = (query) => {

    return Topic.findOne(query).lean()
}

const prepareNewTopic = (topic) => {
    const parsedTopic = parseTopic(topic)

    if (!parsedTopic) throw new Error(`empty Topic to create`)

    if (!parsedTopic.name) throw new Error(`topic.name is require`)

    if (!parsedTopic.category) throw new Error(`topic.category is require`)

    parsedTopic.status = 'active'

    return parsedTopic
}


module.exports = async (topic, authPayload) => {
    const parsedTopic = prepareNewTopic(topic)

    const foundTopic = await findOneTopic({name: parsedTopic.name, category: parsedTopic.category})

    if (foundTopic) throw new Error(`Topic ${name} in category:${category} exists`)

    const newTopic = await createOneTopic(parsedTopic, authPayload.user_id)

    return {
        data: {
            topic: newTopic,
        },
    }
}

const {getModel} = require('../../connections/database')
// const parseField = require('../../helpers/parseField')
// const parseSort = require('../../helpers/parseSort')
const UserTopic = getModel('UserTopic')
const UserQuiz = getModel('UserQuiz')
const G12Service = require('../../services/G12Service')

const _caculateRank = async (topicId, sort) => {

    const userTopic = await _findUserTopic(topicId)

    const userQuizzes = await _findUserQuiz(userTopic.map(uT => uT._id))

    const sortValue = sort === 'desc' ? -1 : 1

    return userTopic.map(uT => {
        const {user, _id} = uT

        return Object.assign({
            user,
            score: 0,
            memory_used: 0,
            time: 0,
        }, userQuizzes[_id])
    }).sort((a, b) => {
        const {score: sA, memory_used: mUA, time: tA} = a
        const {score: sB, memory_used: mUB, time: tB} = b

        return ((sA - sB) || (tA - tB) || (mUA - mUB)) * sortValue
    })
}

const _findUserQuiz = async (userTopicIds) => {
    const userQuiz = await UserQuiz.aggregate([
        {
            $match: {
                user_topic: {$in: userTopicIds}
            }
        },
        {
            $group: {
                _id: {user_topic: "$user_topic"},
                score: {$sum: "$result.score"},
                time: {$sum: "$result.time"},
                memory_used: {$sum: "$result.memory_used"},
            }
        }

    ])

    return userQuiz.reduce((prev, uQ) => {
        const {_id, ...result} = uQ

        prev[_id.user_topic] = result

        return prev
    }, {})
}

const _findUserTopic = async (topicId) => {
    return UserTopic.find({
        topic: topicId
    }).select('_id user').lean()
}

const _mapUserDetail = async (ranks, token) => {

    const userIds = ranks.map(r => r.user)

    const {users} = await G12Service.getUsers(userIds, token)

    const userIdMap = users.reduce((prev, user) => {
        const {id} = user

        prev[id] = user

        return prev
    }, {})

    return ranks.map(r => {
        const {user} = r

        const userDetail = userIdMap[user] || {id: user, name: 'System Test G4', userName: 'system_test_g4', email: 'systestg4@codeatest.cf'}

        return Object.assign({}, r, {user: userDetail, user_id: user})
    })

}

module.exports = async (_id, {token}, {sort} = {}) => {
    const parsedSort = ['desc', 'asc'].find((value => value === (sort + '').toLowerCase())) || 'desc'

    const found = await _caculateRank(_id, parsedSort)

    const result = await _mapUserDetail(found, token)

    return {
        data: {
            ranking: result,
        },
        meta: {
            sort: parsedSort,
        }
    }
}

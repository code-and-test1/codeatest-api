const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildQuery = require('./buildQuery')

const Topic = getModel('Topic')

const findTopic = (query, {skip, limit}, {select, sort}) => {

    query.is_deleted = {$ne: true}

    const find = Topic.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const countTopic = query => {
    return Topic.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        findTopic(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countTopic(builtQuery),
    ])

    return {
        data: {
            topics: data,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Topic = getModel('Topic')
const UserTopic = getModel('UserTopic')

const _findById = (_id, select) => {
    const find = Topic.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

const _findUserTopic = (userId, topicId) => {

    return UserTopic.findOne({user: userId, topic: topicId})
}

const _createUserTopic = async (topic, user) => {
    const newTestCase = new UserTopic({topic, user})

    const doc = await newTestCase.save()

    return doc.toJSON()
}

module.exports = async (_id, field, authPayload) => {
    const parsedField = parseField(field)
    const {user_id} = authPayload

    const findTopic = _findById(_id, parsedField)
    const findUserTopic = _findUserTopic(user_id, _id)

    const [found, foundUserTopic] = await Promise.all([findTopic, findUserTopic])

    if (!found) throw new Error(`Topic ${_id} not found`)

    if (foundUserTopic) throw new Error(`this topic has been joined`)

    const userTopic = await _createUserTopic(_id, user_id)

    return {
        data: process.env.NODE_ENV !== 'production' ? {
            topic: found,
            user_topic: userTopic,
        } : true,
        meta: {
            field: parsedField,
        }
    }
}

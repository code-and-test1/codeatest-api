exports.getList = require('./getList')

exports.getById = require('./getById')

exports.getRanking = require('./getRanking')

exports.joinById = require('./joinById')

exports.getQuizById = require('./getQuizById')

exports.create = require('./create')

exports.updateById = require('./updateById')

exports.deleteById = require('./deleteById')

const Checker = require('../../helpers/Checker')
const allowStatuses = ['active', 'in-active']

module.exports = (topic) => {
    if (!topic) return null

    const parsed = {}

    if (topic.hasOwnProperty('name')) {
        parsed.name = Checker.isString(topic.name) ? topic.name.trim() : null
    }

    if (topic.hasOwnProperty('description')) {
        parsed.description = Checker.isString(topic.description) ? topic.description.trim() : null
    }

    if (topic.hasOwnProperty('status')) {
        parsed.status = allowStatuses.includes(topic.status) ? topic.status : null
    }

    return Object.assign({}, topic, parsed)
}

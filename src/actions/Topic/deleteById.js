const {getModel} = require('../../connections/database')

const Topic = getModel('Topic')

const updateTopicById = (_id, updateTopic) => {
    return Topic
        .findByIdAndUpdate(
            _id,
            Object.assign(updateTopic, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id) => {
    const oldTopic = await Topic.findById(_id).lean()

    if (!oldTopic) throw new Error(`Topic:${_id} does not exists`)

    await updateTopicById(_id, {
        is_deleted: true,
    })

    return {
        data: true,
    }
}

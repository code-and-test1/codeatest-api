const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildQuery = require('./buildQuery')

const TestCase = getModel('TestCase')

const findTestCase = (query, {skip, limit}, {select, sort}) => {

    const find = TestCase.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const countTestCase = query => {
    return TestCase.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        findTestCase(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countTestCase(builtQuery),
    ])

    return {
        data: {
            testCases: data,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

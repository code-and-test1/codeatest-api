const Promise = require('bluebird')
// const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
// const buildQuery = require('./buildQuery')

const UserTopic = getModel('UserTopic')
const UserQuiz = getModel('UserQuiz')

const _findUserTopic = (userId, topicId) => {
    return UserTopic.findOne({
        user: userId,
        topic: topicId,
        is_deleted: {
            $ne: true,
        },
    }).select('_id').lean()
}

const _findQuizzes = async (query, select, sort) => {

    const find = UserQuiz.find(query)
        .populate({
            path: 'quiz',
            select: select,
        })
        .sort(sort || {created_at: 1})

    return find.lean()
}

const _countQuiz = query => {
    return UserQuiz.countDocuments(query)
}

module.exports = async (userId, topicId, {field, sort}) => {
    const parsedField = parseField(field)
    const parsedSort = parseSort(sort)

    const userTopic = await _findUserTopic(userId, topicId)

    if (!userTopic) throw new Error(`this User hasn't join Topic ${topicId}`)

    const builtQuery = {
        user_topic: userTopic._id,
        // is_deleted: false,
    }

    const [userQuizzes, total] = await Promise.all([
        _findQuizzes(builtQuery, parsedField),
        _countQuiz(builtQuery),
    ])

    const quizzes = userQuizzes.map(userQuiz => {
        return userQuiz.quiz
    })

    return {
        data: {
            quizzes: quizzes,
            // user_quizzes: userQuizzes.map(userQuiz => {
            //     return Object.assign({}, userQuiz, {quiz: userQuiz.quiz._id})
            // }),
            // user_topc: userTopic,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            // page: parsedPaging.page,
            // limit: parsePaging.limit,
        }
    }
}

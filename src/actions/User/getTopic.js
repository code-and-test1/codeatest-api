const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
// const buildQuery = require('./buildQuery')

const UserTopic = getModel('UserTopic')
const Topic = getModel('Topic')

const findTopic = async (query, {skip, limit}, {select, sort}) => {

    const find = UserTopic.find(query)
        .populate({
            path: 'topic',
            select: select,
        })
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    return find.lean()
}

const countTopic = query => {
    return UserTopic.countDocuments(query)
}

module.exports = async (userId, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const builtQuery = {
        user: userId,
        is_deleted: {$ne: true},
    }

    const [userTopics, total] = await Promise.all([
        findTopic(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countTopic(builtQuery),
    ])

    const topics = userTopics.map(userTopic => {
        return userTopic.topic
    })

    return {
        data: {
            topics: topics,
            // user_topics: userTopics.map(userTopic => {
            //     return Object.assign({}, userTopic, {topic: userTopic.topic._id})
            // }),
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

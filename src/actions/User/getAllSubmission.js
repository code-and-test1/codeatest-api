const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildQuery = require('../Submit/buildQuery')

const Submit = getModel('Submit')
const UserTopic = getModel('UserTopic')
const UserQuiz = getModel('UserQuiz')
const Quiz = getModel('Quiz')

const findSubmit = async ({query, userId}, {skip, limit}, {select, sort}) => {
    const allJoinedQuizId = await findAllJoinedQuizId(userId)

    query.user_quiz = {
        $in: allJoinedQuizId,
    }

    query.is_deleted = {$ne: true}


    const find = Submit.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})
        .populate({
            path : 'user_quiz',
            select: '_id',
            populate : {
              path : 'quiz',
              select: ['_id', 'name'],
            }
          })

    if (select) find.select(select)

    return find.lean()
}

const countSubmit = query => {
    return Submit.countDocuments(query)
}

const findAllJoinedQuizId = async (userId) => {

    const userTopic = await UserTopic.find({
        user: userId
    }).select('_id').lean()

    const userQuiz = await UserQuiz.find({
        user_topic: {$in: userTopic.map(uT => uT._id)}
    })
        // .populate({
        //     path: 'quiz',
        //     select: '_id',
        // })
        .select('_id').lean()

    const allJoinedQuizId = userQuiz.map(uQ => uQ._id).filter(Boolean)

    return allJoinedQuizId
}

module.exports = async ({query, userId}, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total, inforQuiz] = await Promise.all([
        findSubmit({query: builtQuery, userId: userId}, parsedPaging, {select: parsedField, sort: parsedSort}),
        countSubmit(builtQuery),
        // findQuiz({query: builtQuery, userId: userId}),
    ])

    return {
        data: {
            submits: data,
            quiz: inforQuiz
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}
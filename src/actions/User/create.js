const {getModel} = require('../../connections/database')
const parseUser = require('./parseData')
const _ = require('lodash')

const User = getModel('User')

const createOneUser = async quiz => {
    const newUser = new User(quiz)

    const doc = await newUser.save()

    return doc.toJSON()
}

const prepareNewUser = (user) => {
    // const parsedUser = parseUser(user)

    // if (!parsedUser) throw new Error(`empty User to create`)

    // if (!parsedUser.input) throw new Error(`user.input is require`)

    // if (!parsedUser.output) throw new Error(`user.output is require`)

    // if (!parsedUser.category) throw new Error(`user.category is require`)

    // return parsedUser

    return _.pick(user, ['name', 'group'])
}


module.exports = async (user) => {
    const parsedUser = prepareNewUser(user)

    const newUser = await createOneUser(parsedUser)

    return {
        data: {
            user: newUser,
        },
    }
}

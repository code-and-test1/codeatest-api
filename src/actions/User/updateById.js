const parseTestCase = require('./parseData')
const {getModel} = require('../../connections/database')

const TestCase = getModel('TestCase')

const updateTestCaseById = (_id, updateTestCase) => {
    return TestCase
        .findByIdAndUpdate(
            _id,
            Object.assign(updateTestCase, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, testCase) => {
    const parsedTestCase = parseTestCase(testCase)

    if (!parsedTestCase) throw new Error(`empty TestCase update`)

    const oldTestCase = await TestCase.findById(_id).lean()

    if (!oldTestCase) throw new Error(`testCase:${_id} does not exists`)

    const newTestCase = await updateTestCaseById(_id, parsedTestCase)

    if (!newTestCase) throw new Error(`TestCase ${_id} not exists`)

    return {
        data: {
            testCase: newTestCase,
        },
    }
}

exports.getList = require('./getList')

// exports.getById = require('./getById')

exports.create = require('./create')

// exports.updateById = require('./updateById')

exports.getTopic = require('./getTopic')

exports.getQuizByTopic = require('./getQuizByTopic')

exports.getAllSubmission = require('./getAllSubmission')

exports.getSubmissionOfQuiz = require('./getSubmissionOfQuiz')

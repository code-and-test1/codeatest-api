const {getModel} = require('../../connections/database')

const Quiz = getModel('Quiz')

const updateQuizById = (_id, updateQuiz) => {
    return Quiz
        .findByIdAndUpdate(
            _id,
            Object.assign(updateQuiz, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id) => {
    const oldQuiz = await Quiz.findById(_id).lean()

    if (!oldQuiz) throw new Error(`quiz:${_id} does not exists`)

    await updateQuizById(_id, {
        is_deleted: true,
    })

    return {
        data: true,
    }
}

const Promise = require('bluebird')
const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Quiz = getModel('Quiz')
const UserQuiz = getModel('UserQuiz')
const UserTopic = getModel('UserTopic')

const _findById = (_id, select) => {
    const find = Quiz.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

const _createUserQuiz = async (quizId, userTopicId) => {

    const newTestCase = new UserQuiz({quiz: quizId, user_topic: userTopicId})

    const doc = await newTestCase.save()

    return doc.toJSON()
}

const _findUserTopic = async (userId) => {
    return UserTopic.findOne({user: userId})
}

const _findUserQuiz = async (userTopicId, quizId) => {

    return UserQuiz.findOne({user_topic: userTopicId, quiz: quizId})
}

module.exports = async (quizId, authPayload, field) => {
    const parsedField = parseField(field)
    const {user_id} = authPayload

    const findQuiz = _findById(quizId, parsedField)
    const findUserTopic = _findUserTopic(user_id)

    const [foundQuiz, foundUserTopic] = await Promise.all([findQuiz, findUserTopic])

    if (!foundQuiz) throw new Error(`Quiz ${quizId} not found`)

    if (!foundUserTopic) throw new Error(`this User hasn't join Topic of this Quiz`)

    const foundUserQuiz = await _findUserQuiz(foundUserTopic._id, quizId)

    if (foundUserQuiz) throw new Error(`this quiz has been joined`)

    const userQuiz = await _createUserQuiz(quizId, foundUserTopic._id)

    return {
        data: process.env.NODE_ENV !== 'production' ? {
            user_quiz: userQuiz,
            quiz: foundQuiz,
            user_topic: foundUserTopic,
        } : true,
        meta: {
            field: parsedField,
        }
    }
}

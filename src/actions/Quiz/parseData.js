const Checker = require('../../helpers/Checker')
const allowStatuses = ['active', 'in-active']

module.exports = (quiz) => {
    if (!quiz) return null

    const parsed = {}

    if (quiz.hasOwnProperty('name')) {
        parsed.name = Checker.isString(quiz.name) ? quiz.name.trim() : null
    }

    if (quiz.hasOwnProperty('description')) {
        parsed.description = Checker.isString(quiz.description) ? quiz.description.trim() : null
    }

    if (quiz.hasOwnProperty('content')) {
        parsed.content = Checker.isString(quiz.content) ? quiz.content.trim() : null
    }

    if (quiz.hasOwnProperty('status')) {
        parsed.status = allowStatuses.includes(quiz.status) ? quiz.status : null
    }

    return Object.assign({}, quiz, parsed)
}

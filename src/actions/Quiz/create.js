const {getModel} = require('../../connections/database')
const parseQuiz = require('./parseData')

const Quiz = getModel('Quiz')

const createOneQuiz = async (quiz, quizStat) => {
    quiz.stat = Object.assign({}, quiz.stat, quizStat)
    const newQuiz = new Quiz(quiz)

    const doc = await newQuiz.save()

    return doc.toJSON()
}

const findOneQuiz = (query) => {

    return Quiz.findOne(query).lean()
}

const prepareNewQuiz = (quiz) => {
    const parsedQuiz = parseQuiz(quiz)

    if (!parsedQuiz) throw new Error(`empty Quiz to create`)

    if (!parsedQuiz.name) throw new Error(`quiz.name is require`)

    if (!parsedQuiz.topic) throw new Error(`quiz.topic is require`)

    parsedQuiz.status = 'active'

    return parsedQuiz
}


module.exports = async (quiz, {user_id}) => {
    const parsedQuiz = prepareNewQuiz(quiz)

    const foundQuiz = await findOneQuiz({name: parsedQuiz.name, topic: parsedQuiz.topic})

    if (foundQuiz) throw new Error(`Quiz ${name} in topic:${topic} exists`)

    const quizStat = {
        author: user_id
    }

    const newQuiz = await createOneQuiz(parsedQuiz, quizStat)

    return {
        data: {
            quiz: newQuiz,
        },
    }
}

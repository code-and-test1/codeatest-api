const {getModel} = require('../../connections/database')
const Checker = require('../../helpers/Checker')
const TestCase = getModel('TestCase')
const Quiz = getModel('Quiz')

const createTestCase = async (testCases) => {

    return TestCase.create(testCases)
}

const prepareNewTestCase = (testCase) => {

    const parsedTestCase = {quiz: testCase.quiz}

    if (!Checker.isString(testCase.input)) throw new Error(`testCase.input must be string`)
    parsedTestCase.input = testCase.input

    if (!Checker.isString(testCase.output)) throw new Error(`testCase.output must be string`)
    parsedTestCase.output = testCase.output

    return parsedTestCase
}

const findQuiz = (quizId) => {

    return Quiz.findById(quizId).select('_id').lean()
}

module.exports = async (quizId, {test_cases: testCases}) => {
    const quizFound = await findQuiz(quizId)

    if (!quizFound) throw new Error(`quiz ${quizId} not found`)

    const parsedTestCases = testCases.map(testCase => {
        if (!testCase) throw new Error(`empty test_case`)

        return prepareNewTestCase(Object.assign(testCase, {quiz: quizId}))
    })

    const newTestCase = await createTestCase(parsedTestCases)

    return {
        data: {
            test_cases: Array.isArray(newTestCase) ? newTestCase : [newTestCase],
        },
    }
}

const Promise = require('bluebird')
const {getModel} = require('../../connections/database')
const parseSubmit = require('../Submit/parseData')
const Uploader = require('../../services/Uploader')

const Submit = getModel('Submit')
const Quiz = getModel('Quiz')
const UserTopic = getModel('UserTopic')
const UserQuiz = getModel('UserQuiz')

const createOneSubmit = async quiz => {
    const newSubmit = new Submit(quiz)

    const doc = await newSubmit.save()

    return doc.toJSON()
}

const prepareNewSubmit = (submit) => {
    const parsedSubmit = parseSubmit(submit)

    if (!parsedSubmit) throw new Error(`empty Submit to create`)

    // if (!parsedSubmit.user_quiz) throw new Error(`submit.user_quiz is require`)

    // if (!parsedSubmit.quiz) throw new Error(`submit.quiz is require`)

    // if (!parsedSubmit.url) throw new Error(`submit.url is require`)

    if (!parsedSubmit.language) throw new Error(`submit.language is require`)

    parsedSubmit.status = 'pending'

    return parsedSubmit
}

const _uploadCode = async (file, submit, information) => {

    const {userId, quizId} = information

    const {language} = submit
    const {filename, path} = file

    const destination = `code/${userId}/${filename}.${language}`

    const uploadedFile = await Uploader.uploadFile(path, {destination: destination, clean: true})

    return uploadedFile
}

const _findUserQuiz = async (userId, quizId) => {

    const userTopic = await UserTopic.findOne({
        user: userId,
    }).select('_id user topic').lean()

    if (!userTopic) throw new Error(`This User hasn't join Topic of this Quiz`)

    const userQuiz = await UserQuiz.findOne({
        user_topic: userTopic._id,
        quiz: quizId,
    }).select('quiz').lean()

    if (!userQuiz) throw new Error(`This User hasn't join this Quiz`)

    return userQuiz
}

const _findQuiz = (quizId) => {
    return Quiz.findById(quizId).select('_id limit').lean()
}

const _ableToSubmit = async (quiz, userQuiz) => {
    const {_id: userQuizId} = userQuiz
    const submittedCount = await Submit.count({user_quiz: userQuizId})

    const {limit} = Object.assign({}, quiz)

    const {submit} = Object.assign({submit: 0}, limit)

    if (submit !== 0 && submittedCount >= submit) throw new Error(`You have reach limit submit`)

    return true
}

module.exports = async (quizId, submit, file, authPayload) => {
    const parsedSubmit = prepareNewSubmit(submit)
    const {user_id} = authPayload

    const findQuiz = _findQuiz(quizId)
    const findUserQuiz = _findUserQuiz(user_id, quizId)
    // await _checkUserIsSameAsUserQuiz('5fadeb3db2de4f60def024f9', parsedSubmit.user_quiz)

    const [foundQuiz, foundUserQuiz] = await Promise.all([findQuiz, findUserQuiz])

    if (!foundQuiz) throw new Error(`Quiz ${quizId} not found`)
    if (!foundUserQuiz) throw new Error(`You hasn't join this quiz yet`)

    await _ableToSubmit(foundQuiz, foundUserQuiz)

    parsedSubmit.user_quiz = foundUserQuiz._id
    // console.log(parsedSubmit)

    const information = {quizId, userId: user_id}

    const uploadFile = await _uploadCode(file, parsedSubmit, information)

    if (!uploadFile) throw new Error(`Upload file to google fail?`)

    parsedSubmit.upload_file = uploadFile

    // const foundSubmit = await findOneSubmit({name: parsedSubmit.name, category: parsedSubmit.category})

    // if (foundSubmit) throw new Error(`Submit ${name} in category:${category} exists`)

    const newSubmit = await createOneSubmit(parsedSubmit)

    return {
        data: {
            submit: newSubmit,
            quiz: foundQuiz,
            user_quiz: foundUserQuiz,
        },
    }
}

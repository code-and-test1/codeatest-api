const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Quiz = getModel('Quiz')
const TestCase = getModel('TestCase')

const _findById = (_id, select) => {
    const find = Quiz.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

const _findTestCases = (quizId) => {
    return TestCase.find({quiz: quizId})
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    if (!_id) throw new Error(`null quiz_id`)

    const [found, testCases] = await Promise.all([_findById(_id, parsedField), _findTestCases(_id)])

    if (!found) throw new Error(`Quiz ${_id} not found`)

    return {
        data: {
            quiz: Object.assign(found, {test_cases: testCases}),
        },
        meta: {
            field: parsedField,
        }
    }
}

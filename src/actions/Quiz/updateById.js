const parseQuiz = require('./parseData')
const {getModel} = require('../../connections/database')

const Quiz = getModel('Quiz')

const updateQuizById = (_id, updateQuiz) => {
    return Quiz
        .findByIdAndUpdate(
            _id,
            Object.assign(updateQuiz, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, quiz) => {
    const parsedQuiz = parseQuiz(quiz)

    if (!parsedQuiz) throw new Error(`empty Quiz update`)

    const oldQuiz = await Quiz.findById(_id).lean()

    if (!oldQuiz) throw new Error(`quiz:${_id} does not exists`)

    // const mergedQuiz = Object.assign(oldQuiz, parsedQuiz)

    // const foundQuiz = await findOneQuiz({name: mergedQuiz.name, topic: mergedQuiz.topic})

    // if (foundQuiz) throw new Error(`Quiz ${name} in topic:${topic} exists`)

    const newQuiz = await updateQuizById(_id, parsedQuiz)

    if (!newQuiz) throw new Error(`Quiz ${_id} not exists`)

    return {
        data: {
            quiz: newQuiz,
        },
    }
}

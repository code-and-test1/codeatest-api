const {getModel} = require('../../connections/database')
// const parseField = require('../../helpers/parseField')
// const parseSort = require('../../helpers/parseSort')
// const Quiz = getModel('Quiz')
const G12Service = require('../../services/G12Service')
const UserQuiz = getModel('UserQuiz')

const _caculateRank = async (quizId, sort) => {

    const userQuizzes = await _findUserQuiz(quizId, sort)

    return userQuizzes.map(uQ => {
        const {user_topic, result} = uQ

        return Object.assign(result, {user: user_topic.user})
    })
}

const _findUserQuiz = async (quizId, sort) => {
    const sortValue = sort === 'desc' ? -1 : 1

    const userQuiz = await UserQuiz.find({
        quiz: quizId
    }).populate({
        path: 'user_topic',
        select: '_id user',
    }).select('_id user_topic result').sort({'result.score': sortValue, 'result.time': sortValue, 'result.memory_used': sortValue}).lean()

    return userQuiz
}

const _mapUserDetail = async (ranks, token) => {

    const userIds = ranks.map(r => r.user)

    const {users} = await G12Service.getUsers(userIds, token)

    const userIdMap = users.reduce((prev, user) => {
        const {id} = user

        prev[id] = user

        return prev
    }, {})

    return ranks.map(r => {
        const {user} = r

        const userDetail = userIdMap[user] || {id: user, name: 'System Test G4', userName: 'system_test_g4', email: 'systestg4@codeatest.cf'}

        return Object.assign({}, r, {user: userDetail, user_id: user})
    })

}

module.exports = async (_id, {token}, {sort} = {}) => {
    const parsedSort = ['desc', 'asc'].find((value => value === (sort + '').toLowerCase())) || 'desc'

    const found = await _caculateRank(_id, parsedSort)

    const result = await _mapUserDetail(found, token)

    return {
        data: {
            ranking: result,
        },
        meta: {
            sort: parsedSort,
        }
    }
}

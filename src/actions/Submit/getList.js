const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildQuery = require('./buildQuery')

const Submit = getModel('Submit')

const findSubmit = (query, {skip, limit}, {select, sort}) => {

    const find = Submit.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const countSubmit = query => {
    return Submit.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [data, total] = await Promise.all([
        findSubmit(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countSubmit(builtQuery),
    ])

    return {
        data: {
            submits: data,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

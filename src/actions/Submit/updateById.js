const parseSubmit = require('./parseData')
const {getModel} = require('../../connections/database')

const Submit = getModel('Submit')

const updateSubmitById = (_id, updateSubmit) => {
    return Submit
        .findByIdAndUpdate(
            _id,
            Object.assign(updateSubmit, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, submit) => {
    const parsedSubmit = parseSubmit(submit)

    if (!parsedSubmit) throw new Error(`empty Submit update`)

    const oldSubmit = await Submit.findById(_id).lean()

    if (!oldSubmit) throw new Error(`submit:${_id} does not exists`)

    // const mergedSubmit = Object.assign(oldSubmit, parsedSubmit)

    // const foundSubmit = await findOneSubmit({name: mergedSubmit.name, category: mergedSubmit.category})

    // if (foundSubmit) throw new Error(`Submit ${name} in category:${category} exists`)

    const newSubmit = await updateSubmitById(_id, parsedSubmit)

    if (!newSubmit) throw new Error(`Submit ${_id} not exists`)

    return {
        data: {
            submit: newSubmit,
        },
    }
}

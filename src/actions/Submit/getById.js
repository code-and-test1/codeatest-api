const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Submit = getModel('Submit')
const Uploader = require('../../services/Uploader')

const _findById = (_id, select) => {
    const find = Submit.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const found = await _findById(_id, parsedField)

    if (!found) throw new Error(`Submit ${_id} not found`)

    const uploadFileUrl = await Uploader.generateSignedUrl(found.upload_file)

    found.upload_file_url = uploadFileUrl

    return {
        data: {
            submit: found,
        },
        meta: {
            field: parsedField,
        }
    }
}

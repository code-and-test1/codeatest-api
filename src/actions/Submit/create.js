const {getModel} = require('../../connections/database')
const parseSubmit = require('./parseData')
const Uploader = require('../../services/Uploader')

const Submit = getModel('Submit')
const Quiz = getModel('Quiz')
const UserTopic = getModel('UserTopic')
const UserQuiz = getModel('UserQuiz')

const createOneSubmit = async quiz => {
    const newSubmit = new Submit(quiz)

    const doc = await newSubmit.save()

    return doc.toJSON()
}

const prepareNewSubmit = (submit) => {
    const parsedSubmit = parseSubmit(submit)

    if (!parsedSubmit) throw new Error(`empty Submit to create`)

    if (!parsedSubmit.user_quiz) throw new Error(`submit.user_quiz is require`)

    // if (!parsedSubmit.quiz) throw new Error(`submit.quiz is require`)

    // if (!parsedSubmit.url) throw new Error(`submit.url is require`)

    if (!parsedSubmit.language) throw new Error(`submit.language is require`)

    parsedSubmit.status = 'pending'

    return parsedSubmit
}

const _uploadCode = async (file, submit, information) => {

    const {userId, quizId} = information

    const {language} = submit
    const {filename, path} = file

    const destination = `code/${userId}/${quizId}/${language}/${filename}`

    const uploadedFile = await Uploader.uploadFile(path, {destination: destination, clean: true})

    return uploadedFile
}

const _findUserQuiz = async (userId, quizId) => {

    const userTopic = await UserTopic.findOne({
        user: userId,
    }).select('_id user topic').lean()

    if (!userTopic) throw new Error(`This User hasn't join Topic of this Quiz`)

    const userQuiz = await UserQuiz.findOne({
        user_topic: userTopic._id,
        quiz: quizId,
    }).select('quiz').lean()

    if (!userQuiz) throw new Error(`This User hasn't join this Quiz`)

    return userQuiz
}

const _findQuiz = (quizId) => {
    return Quiz.findById(quizId).select('_id').lean()
}

module.exports = async (submit, file) => {
    const parsedSubmit = prepareNewSubmit(submit)

    const findQuiz = findQuiz(parsedSubmit.quiz)
    // await _checkUserIsSameAsUserQuiz('5fadeb3db2de4f60def024f9', parsedSubmit.user_quiz)

    const uploadFile = await _uploadCode(file, parsedSubmit)

    if (!uploadFile) throw new Error(`Upload file to google fail?`)

    parsedSubmit.upload_file = uploadFile

    // const foundSubmit = await findOneSubmit({name: parsedSubmit.name, category: parsedSubmit.category})

    // if (foundSubmit) throw new Error(`Submit ${name} in category:${category} exists`)

    const newSubmit = await createOneSubmit(parsedSubmit)

    return {
        data: {
            submit: newSubmit,
        },
    }
}

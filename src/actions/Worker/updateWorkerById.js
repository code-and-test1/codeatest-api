const parseWorker = require('./parseWorker')
const {getModel} = require('../../connections/database')

const Worker = getModel('Worker')

const updateWorkerById = (_id, updateWorker) => {
    return Worker
        .findByIdAndUpdate(
            _id,
            Object.assign(updateWorker, {
                updated_at: Date.now(),
            }),
            {new: true}
        )
        .lean()
}

module.exports = async (_id, worker) => {
    const parsedWorker = parseWorker(worker)

    if (!parsedWorker) throw new Error(`empty Worker update`)

    const newWorker = await updateWorkerById(_id, parsedWorker)

    if (!newWorker) throw new Error(`Worker ${_id} not exists`)

    return {
        data: newWorker,
    }
}

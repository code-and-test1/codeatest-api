const Checker = require('../../helpers/Checker')
const allowStatuses = ['active', 'in-active']

module.exports = (category) => {
    if (!category) return null

    const name = Checker.isString(category.name) ? category.name.trim() : null
    const description = Checker.isString(category.description) ? category.description.trim() : null
    const status = allowStatuses.includes(category.status) ? category.status : null

    return {
        name,
        description,
        status,
    }
}

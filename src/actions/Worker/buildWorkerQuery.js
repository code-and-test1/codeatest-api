const {validateString, validateBoolean} = require('../../helpers/parser')
const parseDateQuery = require('../../helpers/parseDateQuery')
const {escapeStringRegex} = require('../../helpers/parseRegex')

module.exports = (query) => {
    const builtQuery = {}

    if (query.name) builtQuery.name = {'$regex': new RegExp(escapeStringRegex(validateString(query.name)), 'gi')}

    if (query.yaml) builtQuery.yaml = {'$regex': new RegExp(escapeStringRegex(validateString(query.yaml)), 'gi')}

    if (query.is_deleted) builtQuery.status = validateBoolean(query.is_deleted)

    if (query.updated_at) builtQuery.updated_at = parseDateQuery(query.updated_at)

    if (query.created_at) builtQuery.created_at = parseDateQuery(query.created_at)

    return builtQuery
}

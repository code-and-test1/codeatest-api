const {getModel} = require('../../connections/database')
const parseWorker = require('./parseWorker')

const Worker = getModel('Worker')

const createOneWorker = async worker => {
    const newWorker = new Worker(worker)

    const doc = await newWorker.save()

    return doc.toJSON()
}

const findOneWorker = (query) => {

    return Worker.findOne(query).lean()
}

const prepareNewWorker = (worker) => {
    const parsedWorker = parseWorker(worker)

    if (!parsedWorker) throw new Error(`empty Worker to create`)

    if (!parsedWorker.name) throw new Error(`Worker.name is require`)

    if (!parsedWorker.yaml) throw new Error(`Worker.yaml is require`)

    parsedWorker.status = 'active'

    return parsedWorker
}


module.exports = async (Worker) => {
    const parsedWorker = prepareNewWorker(Worker)

    const foundWorker = await findOneWorker({name: parsedWorker.name})

    if (foundWorker) throw new Error(`Worker ${name} exists`)

    const newWorker = await createOneWorker(Worker)

    return {
        data: newWorker,
    }
}

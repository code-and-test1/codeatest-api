const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const Worker = getModel('Worker')

const _findWorkerById = (_id, select) => {
    const find = Worker.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const foundWorker = await _findWorkerById(_id, parsedField)

    if (!foundWorker) throw new Error(`Worker ${_id} not found`)

    return {
        data: foundWorker,
        meta: {
            field: parsedField,
        }
    }
}

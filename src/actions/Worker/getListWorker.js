const Promise = require('bluebird')
const parsePaging = require('../../helpers/parsePaging')
const parseField = require('../../helpers/parseField')
const parseSort = require('../../helpers/parseSort')
const {getModel} = require('../../connections/database')
const buildWorkerQuery = require('./buildWorkerQuery')

const Worker = getModel('Worker')

const findWorkers = (query, {skip, limit}, {select, sort}) => {

    const find = Worker.find(query)
        .skip(skip)
        .limit(limit)
        .sort(sort || {created_at: -1})

    if (select) find.select(select)

    return find.lean()
}

const countWorkers = query => {
    return Worker.countDocuments(query)
}

module.exports = async (query, paging, {field, sort}) => {
    const parsedPaging = parsePaging(paging)
    const builtQuery = buildWorkerQuery(query)
    const parsedSort = parseSort(sort)
    const parsedField = parseField(field)

    const [Workers, total] = await Promise.all([
        findWorkers(builtQuery, parsedPaging, {select: parsedField, sort: parsedSort}),
        countWorkers(builtQuery),
    ])

    return {
        data: {
            Workers: Workers,
        },
        meta: {
            query: builtQuery,
            field: parsedField,
            sort: parsedSort,
            total,
            page: parsedPaging.page,
            limit: parsePaging.limit,
        }
    }
}

const {getModel} = require('../../connections/database')

const TestCase = getModel('TestCase')

module.exports = async (_id) => {
    const oldTestCase = await TestCase.findById(_id).lean()

    if (!oldTestCase) throw new Error(`test_case: ${_id} does not exists`)

    await TestCase.findByIdAndDelete(_id)

    return {
        data: true,
    }
}

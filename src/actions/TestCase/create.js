const {getModel} = require('../../connections/database')
const parseTestCase = require('./parseData')

const TestCase = getModel('TestCase')

const createOneTestCase = async testCase => {
    const newTestCase = new TestCase(testCase)

    const doc = await newTestCase.save()

    return doc.toJSON()
}

const prepareNewTestCase = (testCase) => {
    const parsedTestCase = parseTestCase(testCase)

    if (!parsedTestCase) throw new Error(`empty TestCase to create`)

    if (!parsedTestCase.input) throw new Error(`testCase.input is require`)

    if (!parsedTestCase.output) throw new Error(`testCase.output is require`)

    if (!parsedTestCase.quiz) throw new Error(`testCase.quiz is require`)

    return parsedTestCase
}


module.exports = async (testCase) => {
    const parsedTestCase = prepareNewTestCase(testCase)

    const newTestCase = await createOneTestCase(parsedTestCase)

    return {
        data: {
            testCase: newTestCase,
        },
    }
}

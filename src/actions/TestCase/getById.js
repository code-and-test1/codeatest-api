const {getModel} = require('../../connections/database')
const parseField = require('../../helpers/parseField')
const TestCase = getModel('TestCase')

const _findById = (_id, select) => {
    const find = TestCase.findById(_id)

    if (select) find.select(select)

    return find.lean()
}

module.exports = async (_id, field) => {
    const parsedField = parseField(field)

    const found = await _findById(_id, parsedField)

    if (!found) throw new Error(`TestCase ${_id} not found`)

    return {
        data: {
            testCase: found,
        },
        meta: {
            field: parsedField,
        }
    }
}

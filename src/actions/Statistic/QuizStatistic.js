const parseDateQuery = require("../../helpers/parseDateQuery");
const { getModel } = require("../../connections/database");
const mongoose = require("mongoose");

const Submit = getModel("Submit");

exports.countSubmissionsByQuiz = async (quiz_id, query) => {
  let data = {
    pass: new Array(12).fill(0),
    submit: new Array(12).fill(0),
  };

  if (!query.year) {
    return data;
  }

  let submitsData = await Submit.aggregate([
    {
      $lookup: {
        from: "userquizzes",
        localField: "user_quiz",
        foreignField: "_id",
        as: "user_quiz",
      },
    },
    {
      $match: {
        "user_quiz.quiz": new mongoose.Types.ObjectId(quiz_id),
        created_at: {
          $gte: new Date(query.year, 1, 1),
          $lte: new Date(query.year, 12, 31),
        },
      },
    },
    {
      $group: {
        _id: {
          month: { $month: "$created_at" },
        },
        submitCount: { $sum: 1 },
        passCount: {
          $sum: {
            $cond: { if: { $eq: ["$status", "completed"] }, then: 1, else: 0 },
          },
        },
      },
    },
  ]);

  submitsData.forEach((submitData) => {
    data["submit"][submitData._id.month - 1] = submitData.submitCount;
    data["pass"][submitData._id.month - 1] = submitData.passCount;
  });

  return data;
};